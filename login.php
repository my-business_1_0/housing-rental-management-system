<?php
session_start();
include 'connect.php';
if (($_POST['username']!=null)&&($_POST['userpassword']!=null)){
    $username = $_POST['username'];
    $userpassword = $_POST['userpassword'];
    //从数据库中获取用户名和密码确认登录
    $result = mysqli_query($link,"select * from user where username='$username' and userpassword='$userpassword'");

    $row = mysqli_fetch_assoc($result);

    if ($row['userpassword']==$userpassword){
        //获取头像
        $_SESSION['useravatar']=$row['useravatar'];

        $_SESSION['username']=$username;
        $_SESSION['password']=$userpassword;
        header("Location:house.php"."?username=$username");
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>用户登录</title>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <link rel="stylesheet" href="css/loginCss.css">
</head>

<body>
<nav style="width: 100%;background: white;">
    <div class="loginindexDiv">
        <a href="loginindex.html" class="navContentStyle">安居客</a>
    </div>
</nav>


<div class="content">
    <div class="userLogin">
        <form class="layui-form" action="" method="post" style="width: 450px;height: 254px;margin: 20px auto">

            <h2 class="userLoginH2">账号密码登录</h2>
            <a href="rootlogin.php" class="userLoginP">切换管理员登录</a>

            <input type="text" id="loginUsername" name="username" required lay-verify="required" placeholder="用户名"
                   autocomplete="off" class="layui-icon layui-input inputone">


            <input type="password" id="loginPassword" name="userpassword" required lay-verify="required" placeholder="密码"
                   autocomplete="off" class="layui-icon layui-input inputtow">


            <button type="submit" id="loginBtn" class="layui-btn" lay-submit lay-filter="formDemo" onclick="login()">立即登录</button>

            <!--			<input id="loginBtn" style="text-align: center" type="submit" class="submit layui-btn layui-btn-primary" value="登录">-->

            <div class="userLoginSpan">
                没有账号？<a href="userreg.html">点击注册</a>
            </div>

        </form>
    </div>
</div>

<div class="layui-footer" style="width: 1583px;height: 91px">
    <div style="width: 100%;height: 25px;margin: 10px auto;text-align: center">
        <span><a href="#">关于安居客</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">联系我们</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">用户协议</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">隐私政策</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">房贷计算器</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">最新回答</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">网站地图</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">最新房源</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">其他城市</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">友情链接</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">放心搜</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">推广服务</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">渠道招商</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">58车</a></span>
    </div>
    <div class="footerone" style="width: 100%;height: 25px;margin: 10px auto;text-align: center">
        <span><a href="#">客服服务中心</a></span>
        <span>违法信息举报：  4006209008<span><a href="#">yinsibaohu@58.com</a></span></span>
        <span>Copyright © 2007-2021 www.anjuke.com All Rights Reserved</span>
        <span><a href="#">ICP号：沪 B2-20090008</a></span>
        <span><a href="#">ICP备：沪B2-20090008-1</a></span>
    </div>
</div>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
    });

</script>

</body>
</html>