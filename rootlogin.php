<?php
//启动session函数
session_start();
//连接数据库
include 'connect.php';
//如果管理员账号密码不为空
if (($_POST['rootuser']!=null)&&($_POST['rootpassword']!=null)){

    //接收 post 表单提交上来的数据
    $rootuser = $_POST['rootuser'];
    $rootpassword = $_POST['rootpassword'];

    //从数据库中获取用户名和密码确认登录
    $result = mysqli_query($link,"select * from root where rootuser='$rootuser' and rootpassword='$rootpassword'");

    //结果集中取得一行作为关联数组。
    $row = mysqli_fetch_assoc($result);

    //如果表单提交的密码和数据库的密码一致
    if ($row['rootpassword']==$rootpassword){
        //从结果集中取出管理员账号密码和头像 并设置为 SESSION 函数
        $_SESSION['rootavatar']=$row['rootavatar'];
        $_SESSION['rootuser']=$rootuser;
        $_SESSION['rootpassword']=$rootpassword;

        //跳转到管理页面 page.php
        header("Location:page.php");
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>用户登录</title>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <link rel="stylesheet" href="css/loginCss.css">
</head>

<body>
<nav style="width: 100%;background: white;">
    <div class="loginindexDiv">
        <a href="#" class="navContentStyle">安居客</a>
    </div>
</nav>


<div class="content">
    <div class="userLogin">
            <!--使用 post 方法发送表单数据-->
        <form class="layui-form" action="" method="post" style="width: 450px;height: 254px;margin: 20px auto">

            <h2 class="userLoginH2">管理员登录</h2>
            <a href="login.php" class="userLoginP">切换登录</a>

            <!--name设置为 rootuser 发送数据-->
            <input type="text" id="loginUsername2" name="rootuser" required lay-verify="required" placeholder="请输入管理员账号"
                   autocomplete="off" class="layui-icon layui-input inputone">

            <!--name设置为 rootpassword 发送数据-->
            <input type="password" id="loginPassword2" name="rootpassword" required lay-verify="required" placeholder="请输入管理员密码"
                   autocomplete="off" class="layui-icon layui-input inputtow">


            <button type="submit" id="loginBtn" class="layui-btn" lay-submit lay-filter="formDemo" onclick="login()">立即登录</button>

        </form>
    </div>
</div>

<div class="layui-footer" style="width: 1583px;height: 91px">
    <div style="width: 100%;height: 25px;margin: 10px auto;text-align: center">
        <span><a href="#">关于安居客</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">联系我们</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">用户协议</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">隐私政策</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">房贷计算器</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">最新回答</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">网站地图</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">最新房源</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">其他城市</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">友情链接</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">放心搜</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">推广服务</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">渠道招商</a></span>
        <span style="color: #ccc">|</span>

        <span><a href="#">58车</a></span>
    </div>
    <div class="footerone" style="width: 100%;height: 25px;margin: 10px auto;text-align: center">
        <span><a href="#">客服服务中心</a></span>
        <span>违法信息举报：  4006209008<span><a href="#">yinsibaohu@58.com</a></span></span>
        <span>Copyright © 2007-2021 www.anjuke.com All Rights Reserved</span>
        <span><a href="#">ICP号：沪 B2-20090008</a></span>
        <span><a href="#">ICP备：沪B2-20090008-1</a></span>
    </div>
</div>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;
    });

</script>

</body>
</html>