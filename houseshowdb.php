<?php
//连接数据库
include 'connect.php';

//查询数据库始终为true
$sql = "select * from house where 1=1";

//创建变量接收传递过来的请求,判断如果变量已经设置并且为不是空的话就把它设置成原来的内容，否则变成空
$con=isset($_GET['con'])?$_GET['con']:'';
$content=isset($_GET['content'])?$_GET['content']:'';

//如果发送过来的某个内容等于 db里的某个内容
if ($content=='sousuo'){
    //那么就拼接数据库语句，标题等于标题，价格等于价格，地址等于地址
    $sql.=" and title like '%$con%' or price like '%$con%' or address like '%$con%'";
    //$sql.=" and title like '%$con%' or content like '%$con%'";
}
//把数据库语句放入拼接
$restle = mysqli_query($link,$sql);
//创建一个空数组
$messages=[];
//循环刚才查询出来的数据,并一一放入二维数组
while ($row=mysqli_fetch_assoc($restle)){
    $messages[$row['id']]['id']=$row['id'];
    $messages[$row['id']]['title']=$row['title'];
    $messages[$row['id']]['price']=$row['price'];
    $messages[$row['id']]['jiegou']=$row['jiegou'];
    $messages[$row['id']]['mianji']=$row['mianji'];
    $messages[$row['id']]['louceng']=$row['louceng'];
    $messages[$row['id']]['address']=$row['address'];
    $messages[$row['id']]['fdname']=$row['fdname'];
    $messages[$row['id']]['img']=$row['img'];
}
//把数组转换为 json 格式
echo json_encode($messages);
