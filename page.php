<?php
//启动 session
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>房产信息管理</title>
    <script src="layui-v2.6.8/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <script src="layui-v2.6.8/layui/layui.js"></script>

    <script src="bootstrap/js/jquery/2.0.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet"/>
    <script src="bootstrap/js/bootstrap/3.3.6/bootstrap.min.js"></script>
</head>
<style>
    div.tableDiv table td{
        vertical-align: middle!important;
    }
    div.tableDiv td{
        text-align: center;
    }
    input.logAndSearchInput{
        border: 1px solid #ccc;
        border-radius: 3px 0 0 3px;
        color: #999;
        font-size: 13px;
        height: 33px;
        padding: 7px 0 7px 13px;
        width: 467px;
        vertical-align: text-top;
    }
    input.logAndSearchButton{
        background: #f60;
        border: 0;
        border-radius: 0 3px 3px 0;
        color: #fff;
        font-size: 18px;
        height: 33px;
        margin-left: -1px;
        width: 120px;
        vertical-align: text-top;
    }
</style>
<body>
<nav style="width: 100%">
    <ul class="layui-nav" lay-filter="">
        <li class="layui-nav-item"><h1 style="font-size: 24px;color: white">房产信息管理</h1></li>

        <li class="layui-nav-item layui-nav-item"><a href="page.php" style="text-decoration: none">房产信息管理</a></li>
        <li class="layui-nav-item"><a href="rootreservestate.php" style="text-decoration: none">会员预约管理</a></li>
        <li class="layui-nav-item" style="width: auto;float: right">
                <!--获取管理员账号和头像并把它放进 img的src属性里 和 a标签 -->
            <a href="javascript:;" style="color: white"><img src="<?php echo $_SESSION['rootavatar']; ?>" class="layui-nav-img"><?php echo $_SESSION['rootuser']; ?></a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a href="rootloginout.php">退出登录</a></dd>
            </dl>
        </li>
    </ul>
</nav>

<form action="delete.php" method="post">
    <div style="width: 1600px;margin: 0 auto" class="tableDiv">
        <table class='layui-table' lay-skin="line">
            <thead>
            <tr>
                <input type="text" class="logAndSearchInput" style="margin-left: 50px" id="con" placeholder="请输入小区名称、地址...">
                <input type="button" class="logAndSearchButton layui-btn" id="sousuo" value="搜索">
            </tr>
            <div style="margin-top: 20px">
                <tr>
                    <td class="titleone"><button type="submit" class="layui-btn layui-btn-xs layui-btn-danger" value="批量删除">批量删除</button></td>
                    <td class="titleone">id</td>
                    <td class="titleone">标题</td>
                    <td class="titleone">价格</td>
                    <td class="titleone">结构</td>
                    <td class="titleone">面积</td>
                    <td class="titleone">楼层</td>
                    <td class="titleone">地址</td>
                    <td class="titleone">房东姓名</td>
                    <td class="titleone">房屋封面</td>
                    <td class="titleone"><a  href="addto.html?id='.$row['id'].'"><span class="layui-btn layui-btn-sm layui-btn-normal">添加房屋信息</span></a></td>
                    <td class="titleone"></td>
                </tr>
            </div>
                </thead>
                <tbody id="tbody1">
<!--                .$value['id'].-->
            <?php
            //连接数据库
            include "connect.php";

            //查询名为 house 的表，计算id的行数 给它一个别名为 c ，并对 id 进行升序
            $result = mysqli_query($link, "select count(id) as c from house order by id asc");

            //结果集中取得一行作为关联数组,并把它赋值给 $data 变量
            $data = mysqli_fetch_assoc($result);

            //把计算出来的结果赋值给$count变量
            $count = $data['c'];

            //检测变量是否设置并且不是空，如果成立则把它设置为整型数字，如果不成立则变成 1。设置一个 $page 变量接收判断
            $page = isset($_GET['page']) ? (int)($_GET['page']) : 1;

            //设置一个变量 $num 赋值为 6,作用是
            $num = 6;

            //向上舍入最近的整数(count计算出的结果 除以 $page变量(6))，再创建一个变量 $total 来接收计算结果
            $total = ceil($count / $num);

            //判断如果点击上一页那么它就返回第一页，禁止出现 -1 页的情况
            if ($page <= 1) {
                $page = 1;
            }
            //判断如果点击下一页那么它就返回最后一页，禁止出现 比最后一页大的情况
            if ($page >= $total) {
                $page = $total;
            }

            //创建一个变量 $offset 接收 ($page -1) 乘以 $num(6)
            $offset = ($page - 1) * $num;

            //再次进行查询名为 house 的表并进行升序排序
            $result = mysqli_query($link, "select * from house order by id asc limit $offset,$num");


            //如果以上的查询语句成立 与 返回结果集中行的数量成立
            if ($result && mysqli_num_rows($result)) {

                //那么就进行循环 $result 变量 并创建一个 $row 变量来接收
                while ($row = mysqli_fetch_assoc($result)) {
                    echo '
            <tr id="tr1" style="width: 1013px;height: 67px;text-align: center">
                    <!-- 把循环出来的某条行数中 取出一个个字段里的数据分别放入 td 或 属性里 -->
                    <!-- 复选框name=id[数据库里的某个id] value=用户点击的id -->
                    <td><input type="checkbox" name="id[]" value="' . $row['id'] . '"></td>
                    <td>'.$row['id'].'</td>
                    <td>'.$row['title'].'</td>
                    <td>￥'.$row['price'].'</td>
                    <td>'.$row['jiegou'].'</td>
                    <td>'.$row['mianji'].'</td>
                    <td>'.$row['louceng'].'</td>
                    <td>'.$row['address'].'</td>
                    <td>'.$row['fdname'].'</td>
                    <td><img width="50px" src="'.$row['img'].'"></td>
                    
                    <!-- 设置一个按钮并把 点击到的某个id 把数据发送到 edit.php 进行 -->
                    <td><a  href="edit.php?id='.$row['id'].'">
                            <span class="layui-btn layui-btn layui-btn-sm">修改</span></a></td>
                            
                            <!-- 设置一个按钮并把 点击到的某个id 发送到 delete.php 进行删除 -->
                    <td><a  href="delete.php?id='.$row['id'] .'"onclick="delete()" id="deleteOne" name="delete">
                            <span class="layui-btn layui-btn-danger layui-btn-sm">删除</span></a></td>
            </tr>
            ';
                }
                echo '<div>';
            echo '<table class="layui-table" lay-skin="line"><tr>';
            echo '
                    <td><a class="text-danger" href="page.php?page=1">首页</a></td>
                    
                    <td><a class="text-danger" href="page.php?page=' . ($page - 1) . '">上一页</a></td>
            ';
                    //进行循环判断数据的总数,再把它显示第 1,2,3,4... 页
                    for ($i = 1; $i <= $total; $i++) {
                        echo '<td><a class="text-info" href="page.php?page=' . $i . '">' . $i . '</a></td>';
                    }
            echo '
                    <td><a class="text-danger" href="page.php?page=' . ($page + 1) . '">下一页</a></td>
                    <td><a class="text-danger" href="page.php?page=' . $total . '">尾页</a></td>
                    <td>当前是第' . $page . '页  共' . $total . '页</td>
            ';
            echo '</tr></table></div>';
            }?>
            </tbody>
        </table>
    </div>
</form>

<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;

        //…
    });

    $("con").keyup(function () {
        let con = $("#con").val();
        str='';
        console.log(con);
        $.ajax({
            type: 'get',
            url: 'seachGame.php',
            data: {"con":con},
            dataType: 'json',
            success:function (data) {
                str="";
                $.each(data,function (key,value) {
                    str="<div style='width: 200px;height: 20px;background-color: #e7e7e7' class='suosoukuang1' id='suosoukuang2'>"+
                            "<a href='origin.php?value.gid' style='font-size:15px;color:black'>"+value.gamaname+"</a>"+
                        "</div>";
                    $("div#suokuang").html(str);
                })
            }
        })
    });

    $(function () {
        //创建一个点击事件绑定到 id为sousuo的button
        $("#sousuo").click(function () {
            //清空内容
            str='';
            //获取inpu框的值(用户输入的值)
            var con=$("#con").val();
            $.ajax({
                //使用 get 把参数用户输入的数据发送到 pageshowdb.php
                type:'get',
                url:'pageshowdb.php',
                data:{"con":con},
                //把 json 格式转换为 js 对象
                dataType:'json',
                //当请求完成时执行以下方法
                success:function (data) {
                    //清空内容
                    str="";
                    //用 each 循环 传过来的数据(data) 设置key和value接收每一个数据，再使用标签创建把数据传递进去
                    $.each(data,function (key,value) {
                        str+="<div>"+
                            "<form action='delete.php' method='post'>"+
                            "<tr>"+
                            "<td>"+"<input type='checkbox' name='id[]' value='"+value.id+"' >"+"</td>"+
                            <!--下标-->
                            "<td>"+[key]+"</td>"+
                            <!-- 对象名，对象属性 -->
                            "<td>"+value.title+"</td>"+
                            "<td>"+value.price+"</td>"+
                            "<td>"+value.jiegou+"</td>"+
                            "<td>"+value.mianji+"</td>"+
                            "<td>"+value.louceng+"</td>"+
                            "<td>"+value.address+"</td>"+
                            "<td>"+value.fdname+"</td>"+
                            "<td>"+"<img style='width: 50px' src="+value.img+">"+"</td>"+
                            "<td>"+"<a href='edit.php?id="+value.id+"'>"+"<span class='layui-btn layui-btn layui-btn-sm'>"+"修改"+"</span>"+"</a>"+"</td>"+
                            "<td>"+"<a href='delete.php?id="+value.id+"'>"+"<span class='layui-btn layui-btn-danger layui-btn-sm'>"+"删除"+"</span>"+"</a>"+"</td>"+
                            "</tr>"+
                            "</form>"+
                            "</div>";
                        //把 tbody 的内容装换成 刚才放入的数据
                        $("tbody#tbody1").html(str);
                    })
                }
            })
        })
    })
</script>
</body>
</html>
