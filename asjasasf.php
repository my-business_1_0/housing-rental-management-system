<?php
include 'connect.php';
session_start();
$house = array();
$i = 0;
$result = mysqli_query($link,"select * from house");
while ($row=mysqli_fetch_assoc($result)){
    $house[$i]['id']=$row['id'];
    $house[$i]['title']=$row['title'];
    $house[$i]['price']=$row['price'];
    $house[$i]['jiegou']=$row['jiegou'];
    $house[$i]['mianji']=$row['mianji'];
    $house[$i]['louceng']=$row['louceng'];
    $house[$i]['address']=$row['address'];
    $house[$i]['fdname']=$row['fdname'];
    $house[$i]['img']=$row['img'];
    $house[$i]['zhengzu']=$row['zhengzu'];
    $house[$i]['nanbei']=$row['nanbei'];
    $house[$i]['dtf']=$row['dtf'];

    $i++;
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <script src="layui-v2.6.8/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <script src="kindeditor/kindeditor-all.js"></script>
</head>
<style>
    div.shaixuan01{
        width: 1180px;
        margin: 0px auto;
        border-bottom: 1px dotted #e6e6e6;
        padding: 10px;
        color: #333333;
        font-family: 微软雅黑;
        font-size: 13px;
    }
    div.shaixuan01 span{
        color: #999999;
    }
    div.shaixuan01 a{
        margin: 5px;
    }

    span.huizhang01{
        width: 44px;
        height: 22px;
        border-radius: 16px;
        display: inline-block;
        text-align: center
    }
    div.neirongspan span{
        display: inline-block;
        margin: 6px 10px;
    }
</style>
<body>

<nav>
    <ul class="layui-nav" lay-filter="">
        <li class="layui-nav-item"><h1 style="font-size: 24px;color: white">房产信息管理</h1></li>
        <li class="layui-nav-item layui-nav-item layui-this"><a href="">房产信息管理</a></li>
        <li class="layui-nav-item"><a href="">会员预约管理</a></li>
        <li class="layui-nav-item" style="float: right">
            <a href="javascript:;" style="color: white"><img src="img/houseBackground.jpg" class="layui-nav-img"><?php echo $_SESSION['username']; ?></a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a href="loginout.php">退出登录</a></dd>
            </dl>
        </li>
    </ul>
</nav>

<div class="bigDiv">
    <div>
        <input type="text" id="con" placeholder="输入你想搜索的数据">
        <input type="button" id="sousuo" value="确定">
        <a href="userreserveList.php" style="float: right" class="layui-btn-danger">我的收藏</a>

        <a href="userreservetow.php" style="float: right" class="layui-btn-danger">我的预约</a>
    </div>

    <div style="width: 1180px;height: 194px;margin: 20px auto;border: 1px solid #e6e6e6;">
        <div class="shaixuan01">
            <span>区域：</span>
            <a href="">全部</a>
            <a href="">海城</a>
            <a href="">银海</a>
            <a href="">合浦</a>
            <a href="">铁山港</a>
        </div>
        <div class="shaixuan01">
            <span>租金：</span>
            <a href="">全部</a>
            <a href="">500元以下</a>
            <a href="">500-800元</a>
            <a href="">800-1000元</a>
            <a href="">1000-1500元</a>
            <a href="">1500-2000元</a>
            <a href="">2000-3000元</a>
            <a href="">3000-5000元</a>
            <a href="">5000元及以上</a>
            <input style="width: 40px" type="text">
            -
            <input style="width: 40px" type="text">元
        </div>
        <div class="shaixuan01">
            <span>房型：</span>
            <a href="">一室</a>
            <a href="">二室</a>
            <a href="">三室</a>
            <a href="">四室</a>
            <a href="">五室及以上</a>
        </div>
        <div class="shaixuan01">
            <span>类型：</span>
            <a href="">整租</a>
            <a href="">合租</a>
        </div>
        <div class="shaixuan01">
            <span>更多筛选：</span>

        </div>
    </div>

    <?php
    foreach ($house as $value){
    echo '
    <div style="width: 1180px;margin: 0 auto">
        <a href="">
            <div style="margin: 0 auto">
                <div style="float: left"><img style="width: 180px;height: 135px" src="" alt=""></div>
                <div style="float: left;" class="myTit">
                    <span style="font-size: 16px" class="" onload="limitWords()"><b>'.$value['title'].'</b></span><br>
                    <span>'.$value['jiegou'].'</span><span>|</span><span>'.$value['mianji'].'</span><span>|</span><span>'.$value['louceng'].'</span><span>'.$value['fdname'].'</span><br>
                    <span>'.$value['address'].'</span><span>银海-金海岸</span><span>银滩大道5号</span><br>
                    <span style="color: #e88a78;background-color: #fcf4f1" class="huizhang01">整租</span>
                    <span style="color: #799374;background-color: #f2f6ed" class="huizhang01">朝南</span>
                    <span style="color: #6682ae;background-color: #f0f4f8" class="huizhang01">有电梯</span>
                </div>
            </div>

            <div style="float: left;padding: 20px 0;margin-left: 210px">
                <span style="display: inline-block;color: #eb5f00;font-size: 22px;text-align: left"><b>'.$value['price'].'<span style="font-size: 13px">元/月</span></b></span><br>
                <button class="layui-btn layui-btn-warm" style="margin: 20px 0">立即预约</button>
            </div>
        </a>
    </div>
    <div style="clear: both"></div>
';
    }?>
</div>

<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;

        //…
    });
    $(function () {
        $("#sousuo").click(function () {
            str='';
            var con=$("#con").val();
            $.ajax({
                type:'get',
                url:'houseshowdb.php?content=sousuo',
                data:{"con":con},
                dataType:'json',
                success:function (data) {
                    str="";
                    $.each(data,function (key,value) {
                        str+=
                            "<div style='width: 1180px;margin: 0 auto'>"+
                            "<div class='layui-row'>"+
                            "<div class='layui-col-md2'>"+
                            "<img class='fwimg' width='180px' height='135px' src="+value.img+">"+
                            "</div>"+
                            "<div class='layui-col-md10' style='padding: 10px 0 0 40px;'>"+
                            "<span style='font-size: 20px;font-weight: bold;' class='fwtitle'>"+value.title+"</span>"+
                            "<span class='fwprice' style='display: block;width: 150px;height: 29px;font-size: 22px;float: right;color: #eb5f00'>"+value.price+"<span>"+"元/月"+"</span>"+"</span>"+

                            "<div style='clear: both'>"+"</div>"+"<br>"+
                            "<div>"+
                            "<span class='fwjiegou'>"+value.jiegou+"</span>"+
                            "<span class='fwmianji'>"+value.mianji+"</span>"+
                            "<span class='fwlouceng'>"+value.louceng+"</span>"+
                            "<span class='fwfdname'>"+value.fdname+"</span>"+
                            "</div>"+
                            "<a href='' class='layui-btn layui-btn-warm' style='float:right;margin:0 10px 0 10px'>"+"立即预约"+"</a>"+
                            "<span class='fwaddress'>"+value.address+"</span>"+"<br>"+
                            "<span class='layui-badge layui-bg-orange'>"+"整租"+"</span>"+
                            "<span class='layui-badge layui-bg-green'>"+"朝南"+"</span>"+
                            "<span class='layui-badge layui-bg-blue'>"+"有电梯"+"</span>"+
                            "</div>"+
                            "</div>"+
                            "</div>";
                        $("div#div1").html(str);
                    })
                }
            })
        })
    });
    function limitWords(txt){
        var str = txt;
        str = str.substr(0,200) + '...';
        return str;
    }
</script>
</body>
</html>
