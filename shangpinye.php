<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="bootstrap/js/jquery/2.0.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet"/>
    <script src="bootstrap/js/bootstrap/3.3.6/bootstrap.min.js"></script>
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <script src="layui-v2.6.8/jquery-3.6.0.min.js"></script>
    <style>
        div.shaixuan01{
            width: 1180px;
            margin: 0px auto;
            border-bottom: 1px dotted #e6e6e6;
            padding: 10px;
            color: #333333;
            font-family: 微软雅黑;
            font-size: 13px;
        }
        div.shaixuan01 span{
            color: #999999;
        }
        div.shaixuan01 a{
            margin: 5px;
        }

        span.huizhang01{
            width: 44px;
            height: 22px;
            border-radius: 16px;
            display: inline-block;
            text-align: center
        }
        div.neirongspan span{
            display: inline-block;
            margin: 6px 10px;
        }
    </style>
</head>
<body>
<div style="width: 1180px;height: 194px;margin: 20px auto;border: 1px solid #e6e6e6;">
    <div class="shaixuan01">
        <span>区域：</span>
        <a href="">全部</a>
        <a href="">海城</a>
        <a href="">银海</a>
        <a href="">合浦</a>
        <a href="">铁山港</a>
    </div>
    <div class="shaixuan01">
        <span>租金：</span>
        <a href="">全部</a>
        <a href="">500元以下</a>
        <a href="">500-800元</a>
        <a href="">800-1000元</a>
        <a href="">1000-1500元</a>
        <a href="">1500-2000元</a>
        <a href="">2000-3000元</a>
        <a href="">3000-5000元</a>
        <a href="">5000元及以上</a>
        <input style="width: 40px" type="text">
        -
        <input style="width: 40px" type="text">元
    </div>
    <div class="shaixuan01">
        <span>房型：</span>
        <a href="">一室</a>
        <a href="">二室</a>
        <a href="">三室</a>
        <a href="">四室</a>
        <a href="">五室及以上</a>
    </div>
    <div class="shaixuan01">
        <span>类型：</span>
        <a href="">整租</a>
        <a href="">合租</a>
    </div>
    <div class="shaixuan01">
        <span>更多筛选：</span>

    </div>
</div>

<div style="width: 1180px;margin: 0 auto">
    <a href="">
        <div style="margin: 0 auto">
            <div style="float: left"><img style="width: 180px;height: 135px" src="" alt=""></div>
            <div style="float: left;" class="neirongspan">
                <span style="font-size: 16px"><b>过冬优选2房1厅为厨2阳台银滩区高端小区万达商圈</b></span><br>
                <span>2室1厅</span><span>|</span><span>100平米</span><span>|</span><span>低层(共30层)</span><span>罗书琼</span><br>
                <span>浩海梧桐</span><span>银海-金海岸</span><span>银滩大道5号</span><br>
                <span style="color: #e88a78;background-color: #fcf4f1" class="huizhang01">整租</span>
                <span style="color: #799374;background-color: #f2f6ed" class="huizhang01">朝南</span>
                <span style="color: #6682ae;background-color: #f0f4f8" class="huizhang01">有电梯</span>
            </div>
        </div>

        <div style="float: left;padding: 20px 0;margin-left: 210px">
            <span style="display: inline-block;color: #eb5f00;font-size: 22px;text-align: left"><b>1500<span style="font-size: 13px">元/月</span></b></span><br>
            <button class="layui-btn layui-btn-warm" style="margin: 20px 0">立即预约</button>
        </div>
    </a>
</div>
<script>
    $(function () {
        $()
    })
</script>
</body>
</html>