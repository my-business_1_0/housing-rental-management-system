<?php
session_start();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>房产信息管理</title>
    <script src="layui-v2.6.8/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <script src="layui-v2.6.8/layui/layui.js"></script>

    <script src="bootstrap/js/jquery/2.0.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet"/>
    <script src="bootstrap/js/bootstrap/3.3.6/bootstrap.min.js"></script>
</head>
<style>
    div.tableDiv table td{
        vertical-align: middle!important;
    }
    div.tableDiv td{
        text-align: center;
    }
    input.logAndSearchInput{
        border: 1px solid #ccc;
        border-radius: 3px 0 0 3px;
        color: #999;
        font-size: 13px;
        height: 33px;
        padding: 7px 0 7px 13px;
        width: 467px;
        vertical-align: text-top;
    }
    input.logAndSearchButton{
        background: #f60;
        border: 0;
        border-radius: 0 3px 3px 0;
        color: #fff;
        font-size: 18px;
        height: 33px;
        margin-left: -1px;
        width: 120px;
        vertical-align: text-top;
    }
</style>
<body>
<nav style="width: 100%;">
    <ul class="layui-nav" lay-filter="">
        <li class="layui-nav-item"><h1 style="font-size: 24px;color: white">房产信息管理</h1></li>
        <li class="layui-nav-item layui-nav-item"><a href="page.php" style="text-decoration: none">房产信息管理</a></li>
        <li class="layui-nav-item"><a href="rootreservestate.php" style="text-decoration: none">会员预约管理</a></li>
        <li class="layui-nav-item" style="float: right">
            <a href="javascript:;" style="color: white;text-decoration: none"><img src="<?php echo $_SESSION['rootavatar'];?>" class="layui-nav-img"><?php echo $_SESSION['rootuser']; ?></a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a href="rootloginout.php">退出登录</a></dd>
            </dl>
        </li>
    </ul>
</nav>
<form action="delete.php" method="post">
    <div style="width: 1600px;margin: 0 auto" class="tableDiv">
        <table class='layui-table' lay-skin="line">
            <thead>
            <tr>
                <input type="text" class="logAndSearchInput" style="margin-left: 50px" id="con" placeholder="请输入小区名称、地址...">
                <input type="button" class="logAndSearchButton layui-btn" id="sousuo" value="搜索">
            </tr>
            <div style="margin-top: 20px">
                <tr>
                    <td class="titleone">id</td>
                    <td class="titleone">用户名</td>
                    <td class="titleone">标题</td>
                    <td class="titleone">价格</td>
                    <td class="titleone">结构</td>
                    <td class="titleone">面积</td>
                    <td class="titleone">楼层</td>
                    <td class="titleone">地址</td>
                    <td class="titleone">房东姓名</td>
                    <td class="titleone">房屋封面</td>
                    <td class="titleone">预约状态</td>
                    <td></td>
                    <td></td>
                </tr>
            </div>
            </thead>
            <tbody id="tbody1">
            <!--                .$value['id'].-->
            <?php

            //查询用户的预约信息并分页显示出来
            include "connect.php";
            $result = mysqli_query($link, "select count(id) as c from reserve order by id asc");
            $data = mysqli_fetch_assoc($result);
            $count = $data['c'];

            $page = isset($_GET['page']) ? (int)($_GET['page']) : 1;
            $num = 6;

            $total = ceil($count / $num);

            if ($page <= 1) {
                $page = 1;
            }
            if ($page >= $total) {
                $page = $total;
            }

            $offset = ($page - 1) * $num;


            $result = mysqli_query($link, "select * from reserve order by id asc limit $offset,$num");
            if ($result && mysqli_num_rows($result)) {
                while ($row = mysqli_fetch_assoc($result)) {
                    echo '
            <tr id="tr1" style="width: 1013px;height: 67px;text-align: center">
                    <td>'.$row['id'].'</td>
                    <td>'.$row['username'].'</td>
                    <td>'.$row['title'].'</td>
                    <td>￥'.$row['price'].'</td>
                    <td>'.$row['jiegou'].'</td>
                    <td>'.$row['mianji'].'</td>
                    <td>'.$row['louceng'].'</td>
                    <td>'.$row['address'].'</td>
                    <td>'.$row['fdname'].'</td>
                    <td><img width="50px" src="'.$row['img'].'"></td>
                    <td>'.$row['state'].'</td>
                    <!-- 管理员可以安排用户看房或取消看房，把点击的某个id分别传递给 rootupdatestate.php 和 quxiao.php -->
                    <td>
                        <a  href="rootupdatestate.php?id='.$row['id'].'&title='.$row['title'].'" onclick="delete()" id="deleteOne" name="delete">
                            <span class="layui-btn layui-btn-warm layui-btn-sm">
                                安排看房
                            </span>
                        </a>
                    </td>
                    
                    
                    
                    <td>
                    <a  href="quxiao.php?id='.$row['id'].'&title='.$row['title'].'" onclick="delete()" id="deleteOne" name="delete">
                            <span class="layui-btn layui-btn-danger layui-btn-sm">
                                取消预约
                            </span>
                        </a>
                    </td>
            </tr>
            ';
                }
                echo '<div>';
                echo '<table class="layui-table" lay-skin="line"><tr>';
                echo '
                    <td><a class="text-danger" href="rootreservestate.php?page=1">首页</a></td>
                    <td><a class="text-danger" href="rootreservestate.php?page=' . ($page - 1) . '">上一页</a></td>
            ';
                for ($i = 1; $i <= $total; $i++) {
                    echo '<td><a class="text-info" href="rootreservestate.php?page=' . $i . '">' . $i . '</a></td>';
                }
                echo '
                    <td><a class="text-danger" href="rootreservestate.php?page=' . ($page + 1) . '">下一页</a></td>
                    <td><a class="text-danger" href="rootreservestate.php?page=' . $total . '">尾页</a></td>
                    <td>当前是第' . $page . '页  共' . $total . '页</td>
            ';
                echo '</tr></table></div>';
            }?>
            </tbody>
        </table>
    </div>
</form>

<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;

        //…
    });
    $(function () {
        $("#sousuo").click(function () {
            str='';
            var con=$("#con").val();
            $.ajax({
                type:'get',
                url:'rootreservestateshowdb.php?content=sousuo',
                data:{"con":con},
                dataType:'json',
                success:function (data) {
                    str="";
                    $.each(data,function (key,value) {
                        str+="<div>"+
                            "<form action='delete.php' method='post'>"+
                                "<tr>"+
                                    "<td>"+[key]+"</td>"+
                                    "<td>"+value.username+"</td>"+
                                    "<td>"+value.title+"</td>"+
                                    "<td>"+value.price+"</td>"+
                                    "<td>"+value.jiegou+"</td>"+
                                    "<td>"+value.mianji+"</td>"+
                                    "<td>"+value.louceng+"</td>"+
                                    "<td>"+value.address+"</td>"+
                                    "<td>"+value.fdname+"</td>"+
                                    "<td>"+"<img style='width: 50px' src="+value.img+">"+"</td>"+
                                    "<td>"+value.state+"</td>"+
                                    "<td>"+"<a href='rootupdatestate.php?id="+value.id+"'>"+"<span class='layui-btn layui-btn-warm layui-btn-sm'>"+"安排看房"+"</span>"+"</a>"+"</td>"+
                                    "<td>"+"<a href='quxiao.php?id="+value.id+"'>"+"<span class='layui-btn layui-btn-danger layui-btn-sm'>"+"取消"+"</span>"+"</a>"+"</td>"+
                                "</tr>"+
                            "</form>"+
                            "</div>";
                        $("tbody#tbody1").html(str);
                    })
                }
            })
        })
    })
</script>
</body>
</html>
