<?php
include 'connect.php';

//1.单行通过get传参的方式向delete.php文件写上对应的id
//2.多行通过Post传参的方式向delete.php文件传递对应的id
//判断如果接收到的id是一个数组，那么把数组分割成字符串并用逗号隔开。否则如果id为数字字符串，那么把它转换成int类型。如果都不成立那么就输出"数据不合法"。
if (is_array($_POST['id'])){
    $id = join(',',$_POST['id']);
}elseif (is_numeric($_GET['id'])){
    $id=(int)$_GET['id'];
}else{
    echo '数据不合法';
}

//
$result = mysqli_query($link,"delete from house where id in ($id)");
if ($result){
    echo '
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <div style="margin: 40px auto;width: 500px;height: 100px">
        <i class="layui-icon layui-icon-face-smile-fine" style="font-size: 30px; color: #1E9FFF;"></i>
        <span style="display: inline-block;font-size: 24px;color: #1E9fff">删除成功,正在返回</span>
    </div>
          ';
    header('refresh:0.5;url=page.php');
}else{
    echo '
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <div style="margin: 40px auto;width: 500px;height: 100px">
        <i class="layui-icon layui-icon-face-surprised" style="font-size: 30px; color: #1E9FFF;"></i>
        <span style="display: inline-block;font-size: 24px;color: #1E9fff">删除失败,正在返回</span>
    </div>
          ';
    header('refresh:0.5;url=page.php');
}