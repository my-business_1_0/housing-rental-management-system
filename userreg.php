<?php
include 'connect.php';
//1.判断重复密码 trim()取出左右两边多余的空格
if (trim($_POST['userpassword'])!=trim($_POST['repassword'])){
    exit("两次密码不一致,返回上一页");
}

//2.准备好写入数据
//获取表单提交过来的用户名和密码，并清空左右空格，再创建变量接收
$username=trim($_POST['username']);
$userpassword=trim($_POST['userpassword']);

//把输入的用户名和密码插入到 user 表
$result = mysqli_query($link,"insert into user(username,userpassword)values ('$username','$userpassword')");

//sql语句成立就显示创建成功再重定向到登录页面，否则显示失败，重定向到注册页面
if ($result){
    echo '
        <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
        <script src="layui-v2.6.8/layui/layui.js"></script>
        <div style="width: 500px;height: 500px;margin: 50px auto;text-align: center">
            <h2><i class="layui-icon layui-icon-face-smile" style="font-size: 30px; color: #1E9FFF;">注册成功，正在前往登录页</i></h2>
        </div>
    ';
    header('refresh:0.5;url=login.php');
}else{
    echo "失败";
    header('refresh:0.5;url=userreg.html');
}
