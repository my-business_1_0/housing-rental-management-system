<?php
include 'connect.php';
session_start();
$username=$_SESSION['username'];

$result=mysqli_query($link,"select * from reserve where username = '$username'");

$house = array();
$i = 0;

while ($row=mysqli_fetch_assoc($result)){
    $house[$i]['id']=$row['id'];
    $house[$i]['username']=$row['username'];
    $house[$i]['title']=$row['title'];
    $house[$i]['price']=$row['price'];
    $house[$i]['jiegou']=$row['jiegou'];
    $house[$i]['mianji']=$row['mianji'];
    $house[$i]['louceng']=$row['louceng'];
    $house[$i]['address']=$row['address'];
    $house[$i]['fdname']=$row['fdname'];
    $house[$i]['img']=$row['img'];
    $house[$i]['state']=$row['state'];
    $i++;
}

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <script src="layui-v2.6.8/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <script src="kindeditor/kindeditor-all.js"></script>
</head>
<style>
    div.bigDiv{
        width: 1013px;
        margin: 0 auto;
    }

</style>
<body>

<nav>
    <ul class="layui-nav" lay-filter="">
        <li class="layui-nav-item layui-nav-item"><a href="house.php">首页</a></li>
        <li class="layui-nav-item"><a href="userreserveList.php">我的收藏</a></li>
        <li class="layui-nav-item layui-this"><a href="userreservetow.php">我的预约</a></li>
        <li class="layui-nav-item" style="float: right">
            <a href="javascript:;" style="color: white"><img src="<?php echo $_SESSION['useravatar']; ?>" class="layui-nav-img"><?php echo $_SESSION['username']; ?></a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a href="loginout.php">退出登录</a></dd>
            </dl>
        </li>
    </ul>
</nav>

<div class="bigDiv">
    <div class="textDiv">
        <div class="layui-card" id="div1">
            <?php
            if ($house!=null) {
                foreach ($house as $value) {
                    echo '
            <div class="layui-card-body">
                <div class="layui-row">
                    <div class="layui-col-md2">
                        <img src="' . $value['img'] . '" class="fwimg" width="180px" height="135px" alt="">
                    </div>
                    <div class="layui-col-md10" style="padding: 10px 0 0 40px">
                        <span style="font-size: 20px;font-weight: bold;"  class="fwtitle" >' . $value['title'] . '</span>
                        <span class="fwprice" style="display: block;width: 150px;height: 29px;font-size: 22px;float: right;color: #eb5f00">' . $value['price'] . '
                            <span>元/月</span></span>
                        <div style="clear: both"></div>
                        <br>
                        <div>
                            <span class="fwjiegou">' . $value['jiegou'] . '</span>
                            <span class="fwmianji">' . $value['mianji'] . '</span>
                            <span class="fwlouceng">' . $value['louceng'] . '</span>
                            <span class="fwfdname">' . $value['fdname'] . '</span>
                        </div>
                        <a id="yuyue" href="" class="layui-btn layui-btn-warm layui-btn-warm" style="float: right">' . $value['state'] . '</a>
                        <span class="fwaddress">' . $value['address'] . '</span>
                    </div>
                </div>
            </div>
            ';
                }
                echo '<a class="layui-btn" href="house.php">返回首页</a>';
            }else{
                echo '
                    <div style="margin: 40px auto;width: 500px;height: 100px">
                        <i class="layui-icon layui-icon-face-surprised" style="font-size: 30px; color: #1E9FFF;"></i>
                        <span style="display: inline-block;font-size: 24px;color: #1E9fff">你还没有任何预约噢</span>
                    </div>
                    <a class="layui-btn" href="house.php">返回首页</a>
                ';
            }
            ?>
        </div>
    </div>

</div>
<script>
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;

        //…
    });
</script>
</body>
</html>
