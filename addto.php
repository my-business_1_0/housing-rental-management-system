<?php
//连接数据库
include 'connect.php';

//获取表单提交上来的数据并创建各种变量分别接收各个字段
$id=(int)$_POST['id'];              //id
$title=trim($_POST['title']);       //标题
$price=trim($_POST['price']);       //价格
$jiegou=trim($_POST['jiegou']);     //结构
$mianji=trim($_POST['mianji']);     //面积
$louceng=trim($_POST['louceng']);   //楼层
$address=trim($_POST['address']);   //地址
$fdname=trim($_POST['fdname']);     //房东姓名
$img=trim($_POST['img']);           //房屋图片

//插入语句 把输入的每个字符和数字分别插入到 house 表里的 各个字段里
$result=mysqli_query($link,"insert into house (title,price,jiegou,mianji,louceng,address,fdname,img)values 
('$title','$price','$jiegou','$mianji','$louceng','$address','$fdname','$img')");

/*
 * 如果 成功插入数据那么就显示添加成功,并使用重定向在0.5秒之后返回到 page.php 管理员首页,
 * 否则显示添加失败,并使用重定向在0.5秒之后返回到 page.php 管理员首页
 */
if ($result){
    echo '
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <div style="margin: 40px auto;width: 500px;height: 100px">
        <i class="layui-icon layui-icon-face-smile-fine" style="font-size: 30px; color: #1E9FFF;"></i>
        <span style="display: inline-block;font-size: 24px;color: #1E9fff">添加成功,正在返回</span>
    </div>
          ';
    header('refresh:0.5;url=page.php');
}else{
    echo '
    <script src="layui-v2.6.8/layui/layui.js"></script>
    <link rel="stylesheet" href="layui-v2.6.8/layui/css/layui.css">
    <div style="margin: 40px auto;width: 500px;height: 100px">
        <i class="layui-icon layui-icon-face-surprised" style="font-size: 30px; color: #1E9FFF;"></i>
        <span style="display: inline-block;font-size: 24px;color: #1E9fff">添加失败,正在返回</span>
    </div>
          ';
    header('refresh:0.5;url=page.php');
}
?>


