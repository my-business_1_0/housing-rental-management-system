/*
 Navicat Premium Data Transfer

 Source Server         : php
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : text

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 19/02/2022 01:30:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for house
-- ----------------------------
DROP TABLE IF EXISTS `house`;
CREATE TABLE `house`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `price` int(11) NULL DEFAULT NULL COMMENT '价格',
  `jiegou` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '房屋结构',
  `mianji` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '面积',
  `louceng` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '楼层',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `fdname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '房东姓名',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '商品封面图片',
  `zhengzu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否整租',
  `nanbei` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否南北',
  `dtf` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否电梯房',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of house
-- ----------------------------
INSERT INTO `house` VALUES (1, '可看海 三房两厅两卫 生活方便 吾悦万达商圈 南北通透', 1850, '3室2厅', '122平米', '低层(共32层)', '森海豪庭', '朱勉勉', 'img/houseimg01.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (2, '中电海湾国际全新大三房出租\r\n中电海湾国际全新大三房出租\r\n', 1400, '3室2厅', '90平米', '中层(共9层)', '中电海湾国际社区', '左海丽', 'img/houseimg02.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (3, '过冬优选2房1厅为厨2阳台银滩区高端小区万达商圈', 1500, '2室1厅', '88平米', '高层(共28层)', '昊天海梧桐', '左海丽', 'img/houseimg03.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (4, '大润发商圈，嘉盛名都，朝南大两房，拎包入住', 900, '2室2厅', '85平米', '高层(共28层)', '奥数的骄傲及', '左海丽', 'img/houseimg04.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (5, '3房2厅一线银滩海景万达商圈杭州市场过冬旅游一Liu', 1500, '3室2厅', '103平米', '高层(共32层)', '北海南岸', '罗书琼', 'img/houseimg05.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (6, '[大树尚宅]银滩海景3房2大厅拎包入住万达银滩区域', 1999, '3室3厅', '125平米', '中层(共31层)', '大唐听海', '罗书琼', 'img/houseimg06.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (7, '贵阳市场旁 次新小区 御海澜庭 采光极好', 650, '1室0厅', '49平米', '低层(共21层)', '御海澜庭', '李根', 'img/houseimg07.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (8, '北京路 宁春城商圈 万汇广场 电梯高层 精装一房 可短租', 900, '1室1厅', '48平米', '高层(共30层)', '万汇广场', '李春代', 'img/houseimg08.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (9, '新大润发旁，宏瑞大厦，一房，出租500元每月', 500, '1室0厅', '45平米', '中层(共18层)', '宏瑞大厦', '黄玲', 'img/houseimg09.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (10, '万达对面，天赐良园精装名宿标配，1房1厅，月租金100...', 1000, '1室1厅', '55平米', '高层(共33层)', '天赐良园', '黄玲', 'img/houseimg10.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (11, '紫薇英伦小镇 全新精装修 南北通透 带地暖 出租', 1388, '2室2厅', '97平米', '中层(共15层)', '紫薇英伦小镇', '郭兰香', 'img/houseimg11.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (12, '九州家园，3房1厅，超低价格', 700, '3室2厅', '89平米', '高层(共22层)', '九州家园', '张玉斌', 'img/houseimg12.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (13, '新大润发，贵兴市场旁边，', 600, '2室1厅', '50平米', '中层(共17层)', '金晖广场', '蔡国美', 'img/houseimg13.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (14, '万达广场新大润发商圈 嘉盛名都 三房两卫', 1000, '3室2厅', '89平米', '中层(共32层)', '嘉盛名都', '左海丽', 'img/houseimg14.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (15, '安琪儿医院对面亿海澜泊湾电梯二房一厅家电家具齐全押金4000', 1500, '2室2厅', '80平米', '中层（共22层）', ' 亿海澜泊湾  ', '赵丽丽', 'img/houseimg15.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (16, '2房2厅1卫厨阳过冬优选海景一线度假养身成熟小区', 2300, '2室2厅', '88平米', '高层(共33层)', ' 皇家海湾公馆', '罗书琼', 'img/houseimg16.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (17, '高层银滩海景豪宅3床2房2厅270度大阳台新装修', 2000, '2室2厅', '105平米', ' 高层(共33层)', ' 逢时黄金海岸', '罗书琼', 'img/houseimg17.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (18, '万达吾悦附近昊海梧桐精装小两房配套齐全拎包入住', 700, '2室1厅', '69平米', ' 高层(共26层)', ' 昊海梧桐', '刘雅俊', 'img/houseimg18.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (19, '2房大厅厨卫阳过冬优房高尔夫球场看海景观1流', 1900, '2室1厅', '83平米', ' 高层(共33层)', ' 嘉和冠山海', '罗书琼', 'img/houseimg19.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (20, '茶亭路高档小区富丽华海域电梯高层精装二房二厅家电家具齐全拎包', 1800, '2室2厅', '85平米', ' 高层(共32层)', ' 富丽华海御 ', '赵丽丽', 'img/houseimg20.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (21, '候鸟湾三房二厅一卫精装修家电家具齐全拎包', 1300, '2室2厅', '80平米', ' 高层(共30层)', ' 候鸟湾', '赵丽丽', 'img/houseimg21.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (22, '北海大道东峰锦绣城 电梯精装 两房南北通透，租金700元', 700, '2室2厅', '88平米', ' 低层(共26层)', ' 东峰锦绣城', '安文芳', 'img/houseimg22.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (23, '江景，海景，恒大名都，吾悦广场，出租，精装两房，有钥匙', 1200, '2室2厅', '72平米', '  中层(共32层)', ' 恒大名都 ', '唐君玲', 'img/houseimg23.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (24, '杭州路市场，万达广场，银滩景区，家电家具齐全，三房，南北通透', 900, '3室2厅', '88平米', ' 高层(共32层)', ' 金域熙城 ', '唐君玲', 'img/houseimg24.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (25, '可短租新大润发商圈丽景佳苑三房1000元一个月 拎包入住', 1000, '3室2厅', '89平米', ' 中层(共16层)', ' 丽景佳苑  ', '黄宇波', 'img/houseimg25.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (26, '恒大名都，精装修两房，朝南，有钥匙，家具家电齐全，吾悦广场', 950, '2室2厅', '71平米', ' 高层(共32层)', ' 恒大名都 ', '唐君玲', 'img/houseimg26.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (27, '广东路海岸华府 三房两厅两卫南北通透租1000一个月', 1000, '3室2厅', '110平方米', '  高层(共18层)', '海岸华府', '张春艳', 'img/houseimg27.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (28, '万达吾悦双商圈 中港城两房两厅一卫精装修南北通透户型方正', 1200, '2室2厅', '75平方米', ' 高层(共17层)', ' 中港城', '马冬梅', 'img/houseimg28.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (29, '恒大御景半岛三房两厅两卫南北通透出租1000元一月拎包入住\r\n', 1000, '3室2厅', '120平方米', ' 高层(共32层)', ' 恒大御景半岛', '黄宇波', 'img/houseimg29.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (30, '海景房出租，森海豪庭两房两厅，精装修。拎包入住\r\n', 1200, '2室2厅', '88平方米', '中层(共26层)', '森海豪庭', '洪玉', 'img/houseimg30.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (31, '45平米单间出租，海景效果非常棒，装修很温馨，长租短租均可', 900, '2室2厅', '45平方米', ' 高层(共33层)', ' 碧桂园北纬21度 ', '史晓艳', 'img/houseimg31.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (32, '吉租 万达附近 干净装修3房 南北对流户型', 750, '3室2厅', '80平方米', ' 中层(共11层)', '九州家园 ', '苏发军', 'img/houseimg32.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (33, '万达广场附近 兆信金悦湾 精装两房\r\n', 1350, '2室1厅', '66平方米', '中层(共32层)', ' 兆信金悦湾', '宋存星', 'img/houseimg33.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (34, '全屋硅藻泥嘉盛名都 豪华装修两房 三台空调', 1100, '2室2厅', '85平方米', '中层(共32层)', ' 嘉盛名都', '宋存星', 'img/houseimg34.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (35, '万达广场附近 万泉城 二区 南北通透 两房', 1200, '2室2厅', '80平方米', ' 高层(共6层)', ' 万泉城2区', '宋存星', 'img/houseimg35.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (36, '高层海景房，碧桂园北纬21度2房800一月，过冬房价钱另议', 1100, '2室2厅', '83平方米', ' 中层(共32层)', '碧桂园北纬21度  ', '付胜江', 'img/houseimg36.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (37, '高档小区，恒大御景半岛3房2卫22楼1500一月，过冬房另议', 1500, '3室2厅', '125平方米', ' 高层(共32层)', ' 恒大御景湾  ', '付胜江', 'img/houseimg37.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (38, '河南路 市政公园旁 中港城 南北通透 大四房家电家具全配', 1800, '4室2厅', '124.8平方米', '高层(共10层)', '中港城', '苏可琼', 'img/houseimg38.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (39, '短租，过冬皇家海湾公馆，邻江望海，银滩附近品质社区，配套完善', 2200, '2室2厅', '89平方米', ' 低层(共28层)', '皇家海湾公馆', '左海丽', 'img/houseimg39.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (40, '万达商圈，阳光充足，绿化率高，环境优雅，交通便利，出入方便', 650, '1室1厅', '64.7平方米', '低层(共18层)', '万泉城1区', '左海丽', 'img/houseimg40.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (41, '银滩片区，阳光充足，环境优雅，交通便利，绿化率高，出入方便。', 700, '2室2厅', '63平方米', ' 高层(共18层)', '枫林蓝湾', '左海丽', 'img/houseimg41.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (42, '大润发商圈，嘉盛名都，南北通透大三房两厅两卫，拎包入住', 1100, '3室2厅', '115平方米', '中层(共32层)', '嘉盛名都', '裴贝然', 'img/houseimg42.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (43, '候鸟过冬房，银滩附近，阳光充足，绿化率高，环境优雅，品质小区', 1000, '1室1厅', '65平方米', '低层(共25层)', '森海豪庭 ', '左海丽', 'img/houseimg43.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (44, '碧桂园北纬21度两室两厅一卫81.6平出租', 4000, '2室1厅', '81.6平方米', ' 低层(共28层)', ' 碧桂园北纬21度', '童桐', 'img/houseimg44.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (45, '世纪科技城北部湾广场长青公园周边配套齐全市中心', 600, '1室0厅', '44平方米', '中层(共27层)', ' 北海世纪科技城  ', '兰秀莉', 'img/houseimg45.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (46, '北部湾西路 欣海大夏 精装两出租', 800, '2室2厅', '69平方米', ' 高层(共18层)', ' 欣海大厦', '钟剑波', 'img/houseimg46.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (47, '万达吾悦商圈 民宿装修2房 拎包就能入住 采光视野开阔 急租', 1300, '2室2厅', '84平方米', '中层(共30层)', ' 森海豪庭', '王宁', 'img/houseimg47.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (48, '万达与大润发双商圈品质小区桐洋家园', 1000, '2室2厅', '86平方米', '高层(共32层)', ' 嘉盛名都  ', '白云云', 'img/houseimg48.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (49, '侨港旁 电梯配套两房招租 交通便利', 800, '2室2厅', '77平方米', '低层(共23层)', ' 海湾茗园 ', '陈嘉艳', 'img/houseimg49.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (50, '一线海景带大露台.海辰国际公寓 4室2厅 出租', 2800, '4室2厅', '120平方米', ' 高层(共32层)', ' 海辰国际公寓  ', '周建伟', 'img/houseimg50.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (51, '新房出租，一线海景房，中间楼层前面无遮挡，押一付一，可以过年', 1400, '2室2厅', '75平方米', ' 中层(共29层)', ' 碧桂园北纬21度', '史晓艳', 'img/houseimg51.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (52, '牛角塘私人楼上海路附近无物业费周边配套齐全随时看房价格优惠', 1400, '5室3厅', '259平方米', '共3层', ' 大山花园 ', '兰秀莉', 'img/houseimg52.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (53, '西南大道与广东路交汇处桐洋新城', 1000, '2室2厅', '88平方米', ' 高层(共34层)', '桐洋新城', '刘俊玲', 'img/houseimg53.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (54, '一线海景房，嘉和冠山海，一房，精装', 700, '1室1厅', '48平方米', '中层(共32层)', ' 嘉和冠山海', '董思佳', 'img/houseimg54.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (55, '万达商圈，东峰世纪公寓，三房两卫，精装修，中高楼层', 900, '3室2厅', '90平方米', '中层(共32层)', ' 东峰世纪公寓 ', '董思佳', 'img/houseimg55.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (56, '2房3床大厅海景舒适房，旅游养身一家人优先选择', 2000, '2室2厅', '95平方米', ' 低层(共32层)', '和居壹海江山  ', '罗书琼', 'img/houseimg56.jpg', '合租', '南北', '有电梯');
INSERT INTO `house` VALUES (57, '急租北海大道金葵市场附近，富馨苑，电梯两房600元', 600, '2室2厅', ' 63.5平方米', '中层(共30层)', ' 富馨苑 ', '安文芳', 'img/houseimg57.jpg', '整租', '南北', '有电梯');
INSERT INTO `house` VALUES (58, '碧桂园北纬21度五室三厅三卫出租', 5500, '5室3厅', '230平方米', '共三层', '碧桂园北纬21度(别墅)', '童桐', 'img/houseimg62.jpg', '整租', '南北', '有电梯');

-- ----------------------------
-- Table structure for reserve
-- ----------------------------
DROP TABLE IF EXISTS `reserve`;
CREATE TABLE `reserve`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `jiegou` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `mianji` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `louceng` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `fdname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 92 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of reserve
-- ----------------------------
INSERT INTO `reserve` VALUES (89, 'aa', '碧桂园北纬21度两室两厅一卫81.6平出租', '4000', '2室1厅', '81.6平方米', ' 低层(共28层)', ' 碧桂园北纬21度', '童桐', 'img/houseimg44.jpg', '待处理');
INSERT INTO `reserve` VALUES (90, 'aa', '2房2厅1卫厨阳过冬优选海景一线度假养身成熟小区', '2300', '2室2厅', '88平米', '高层(共33层)', ' 皇家海湾公馆', '罗书琼', 'img/houseimg16.jpg', '成功');
INSERT INTO `reserve` VALUES (91, 'aa', '一线海景带大露台.海辰国际公寓 4室2厅 出租', '2800', '4室2厅', '120平方米', ' 高层(共32层)', ' 海辰国际公寓  ', '周建伟', 'img/houseimg50.jpg', '待处理');
INSERT INTO `reserve` VALUES (88, 'aa', '文艺装修，拎包入住，押一付一，长租短租，过年不加价，随时入住', '14000', '8室5厅', '75平方米', ' 高层(共29层)', '碧桂园北纬21度 ', '史晓艳', 'img/houseimg58.jpg', '成功');

-- ----------------------------
-- Table structure for root
-- ----------------------------
DROP TABLE IF EXISTS `root`;
CREATE TABLE `root`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rootuser` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '管理员名',
  `rootpassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '管理员密码',
  `rootavatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '管理员预设字段1',
  `rootpresettwo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '管理员预设字段2',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of root
-- ----------------------------
INSERT INTO `root` VALUES (1, 'root', 'root', 'img/rootavatar.jpg', NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户表id',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
  `userpassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户密码',
  `useravatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户收藏',
  `userreserve` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户预约',
  `userid` int(11) NULL DEFAULT NULL COMMENT '用户编号',
  `rootpresettow` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户表预留字段2',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '张三', '123', 'img/userAvatar01.png', NULL, NULL, NULL);
INSERT INTO `user` VALUES (2, '李四', '123', 'img/userAvatar02.png', NULL, NULL, NULL);
INSERT INTO `user` VALUES (3, '王五', '123', 'img/userAvatar03.png', NULL, NULL, NULL);
INSERT INTO `user` VALUES (4, '马六', '123', 'img/userAvatar04.png', NULL, NULL, NULL);
INSERT INTO `user` VALUES (23, 'aa', '123', 'img/userAvatar05.png', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
