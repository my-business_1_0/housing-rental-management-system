<?php
include 'connect.php';

//判断如果发送过来的id是否是数字或者是数字字符串
if (is_numeric($_GET['id'])){
    //如果成立那么把id转化成数字整型
    $id=(int)$_GET['id'];
}
//查询数据库 id = "传过来的id"
$result = mysqli_query($link,"select * from house where id =".$id);
//从结果集中取得所有行作为关联数组
$data = mysqli_fetch_all($result);
//var_dump($data);
?>

<script src="bootstrap/js/jquery/2.0.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet"/>
<script src="bootstrap/js/bootstrap/3.3.6/bootstrap.min.js"></script>

<div style="width: 500px;margin: 100px auto;">
    <!-- 使用post把修改好的数据传递到 update.php -->
    <form action="update.php" role="form" method="post">
        <div class="form-group">
            <lable for="name">标题</lable> <!-- [0][1]是数据库里的1个字段，从0开始算 -->
            <input type="text" value="<?php echo $data[0][1]; ?>" class="form-control" name="title">
        </div>

        <div class="form-group">
            <lable for="name">价格</lable>
            <input type="text" value="<?php echo $data[0][2]; ?>" class="form-control" name="price">
        </div>

        <div class="form-group">
            <lable for="name">结构</lable>
            <input type="text" value="<?php echo $data[0][3]; ?>" class="form-control" name="jiegou">
        </div>

        <div class="form-group">
            <lable for="name">面积</lable>
            <input type="text" value="<?php echo $data[0][4]; ?>" class="form-control" name="mianji">
        </div>

        <div class="form-group">
            <lable for="name">楼层</lable>
            <input type="text" value="<?php echo $data[0][5]; ?>" class="form-control" name="louceng">
        </div>

        <div class="form-group">
            <lable for="name">地址</lable>
            <input type="text" value="<?php echo $data[0][6]; ?>" class="form-control" name="address">
        </div>

        <div class="form-group">
            <lable for="name">房东姓名</lable>
            <input type="text" value="<?php echo $data[0][7]; ?>" class="form-control" name="fdname">
        </div>

        <div class="form-group">
            <lable for="name">房屋封面</lable>
            <input type="text" value="<?php echo $data[0][8]; ?>" class="form-control" name="img">
        </div>

        <input type="hidden" name="id" value="<?php echo $data[0][0];?>">
        <button type="submit" class="btn btn-info">修改</button>
    </form>
</div>